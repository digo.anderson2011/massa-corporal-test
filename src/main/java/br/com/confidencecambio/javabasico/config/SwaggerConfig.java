package br.com.confidencecambio.javabasico.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {    
	
	@Bean
    public Docket filialApi() { 
        return new Docket(DocumentationType.SWAGGER_2)  
          .select()                                  
          .apis(RequestHandlerSelectors.basePackage("br.com.confidencecambio.javabasico"))              
          .paths(PathSelectors.any())          
          .build()
          .apiInfo(apiInfo());
    }

	private ApiInfo apiInfo() {
	    return new ApiInfoBuilder()
            .title("Calcular massa corporal")
            .description("Microserviço calcula a massa corporal, recebendo altura e peso")
            .version("1.0.0")
            .license("Apache License Version 2.0")
            .licenseUrl("https://confidencecambio")
            .contact(new Contact("Anderson Soares","d",""))
            .build();	
	 }
	
}