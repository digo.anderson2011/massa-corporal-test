package br.com.confidencecambio.javabasico.controller;

import br.com.confidencecambio.javabasico.service.CalculaMassaService;
import br.com.confidencecambio.javabasico.vo.ResponseVO;
import br.com.confidencecambio.javabasico.vo.ErrorVO;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CalcularMassaController {

    private CalculaMassaService service;

    public CalcularMassaController(final CalculaMassaService service) {

        this.service = service;
    }

    @RequestMapping(value = "/massaCorporal", method = RequestMethod.GET)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Ok", response = ResponseVO.class),
			@ApiResponse(code = 500, message = "Internal Server Error", response = ErrorVO.class) })
    public ResponseEntity<ResponseVO> calcularMassaCorporal(@RequestParam(value = "altura", required = true) Float altura, @RequestParam(value = "peso", required = true) Float peso) {
        
    	ResponseVO responseVO = service.retornaValorValido(altura,peso);

        return new ResponseEntity<ResponseVO>(responseVO, HttpStatus.OK);
    }
}
