package br.com.confidencecambio.javabasico.service;

import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

import br.com.confidencecambio.javabasico.vo.ResponseVO;

import java.util.Optional;

@Component
public class CalculaMassaService {

    public ResponseVO retornaValorValido(@Nullable Float altura, Float peso) {
        
    	ResponseVO responseVO = new ResponseVO();
    	
    	responseVO.setImc((peso)/(altura*altura));

    	return responseVO;
    }
}
