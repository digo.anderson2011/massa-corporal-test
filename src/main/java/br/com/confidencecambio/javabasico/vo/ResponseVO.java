/**
 *
 */
package br.com.confidencecambio.javabasico.vo;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Anderson Soares
 * @version 1.0
 *
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ResponseVO implements Serializable {

  private static final long serialVersionUID = 6309597593630150709L;

  private Float imc;
}
